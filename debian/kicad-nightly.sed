s/^Exec=\([^ ]*\)\(.*\)$/Exec=\1-nightly\2/g
s/^Icon=\(.*\)$/Icon=\1-nightly/g
s/^Name\([^=]*\)=\(.*\)$/Name\1=\2 (nightly)/g
s/^GenericName\([^=]*\)=\(.*\)$/GenericName\1=\2 (nightly)/g
s/^Comment\([^=]*\)=\(.*\)$/Comment\1=\2 (nightly)/g
s/^StartupWMClass=\([^ ]*\)\(.*\)$/StartupWMClass=\1-nightly\2/g
s/^MimeType=\(.*kicad\)\(.*;\)$/MimeType=\1\2\1-nightly\2/g
