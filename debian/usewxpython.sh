#!/bin/bash
#  Add a pre test to choose if we need to activate
# WXPYTHON
WX=$(wx-config --query-toolkit)
PY=$(python3 -c "import wx; print(wx.version().split(' ')[1])")

if wx-config --toolkit=$PY &> /dev/null; then
    echo "ON"
else
    echo "OFF"
fi
